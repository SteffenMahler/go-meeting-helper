package main

import (
    "fmt"
    "github.com/faiface/beep"
    "github.com/faiface/beep/speaker"
    "github.com/faiface/beep/wav"
    "os"
    "time"
)

func playWavFile(filename string, repeat int) {
    f, err := os.Open(filename)
    if err != nil {
        panic(fmt.Sprintf("sound file \"%s\" is missing", filename))
    }

    streamer, format, err := wav.Decode(f)
    if err != nil {
        panic(err.Error())
    }
    defer streamer.Close()

    speaker.Init(format.SampleRate, format.SampleRate.N(time.Second/10))

    l := beep.Loop(repeat, streamer)

    soundDone := make(chan bool)
    speaker.Play(beep.Seq(l, beep.Callback(func() {
        soundDone <- true
    })))
    <-soundDone
}
