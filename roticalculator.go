package main

import (
    "fmt"
    "fyne.io/fyne/v2"
    "fyne.io/fyne/v2/canvas"
    "fyne.io/fyne/v2/container"
    "fyne.io/fyne/v2/layout"
    "math"
)

func newROTICalculator(width float32, height float32) *fyne.Container {

    ratings := &canvas.Text{
        Alignment: fyne.TextAlignLeading,
        Color:     nil,
        Text:      "0 Ratings",
        TextSize:  10,
    }

    entryRatings := newNumEntry()
    entryRatings.MultiLine = true
    entryRatings.Wrapping = fyne.TextTruncate

    l := container.NewBorder(
        nil,
        nil,
        nil,
        nil,
        container.NewVBox(entryRatings, ratings),
    )

    t := &canvas.Text{
        Text:      "-",
        Alignment: fyne.TextAlignCenter,
        TextStyle: fyne.TextStyle{Bold: true},
        TextSize:  50,
    }
    r := container.NewBorder(
        nil,
        &canvas.Text{Text: "return on time invested", Alignment: fyne.TextAlignCenter, TextSize: 9},
        nil,
        nil,
        container.NewHScroll(t))

    entryRatings.OnChanged = func(str string) {
        s, r := entryRatings.NonEmptyRows()

        ratings.Text = fmt.Sprintf("%2d Ratings", r)
        if r == 1 {
            ratings.Text = fmt.Sprintf("%2d Rating ", r)
        }
        ratings.Refresh()

        if r > 0 {
            t.Text = fmt.Sprintf("%2.1f", math.Round(float64(s)/float64(r)*10)/10)
        } else {
            t.Text = "-"
        }
        t.Refresh()
    }

    c := container.New(layout.NewVBoxLayout(), r, l)
    c.Resize(fyne.NewSize(width, height))

    return c
}
