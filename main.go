package main

import (
    "fyne.io/fyne/v2"
    "fyne.io/fyne/v2/app"
    "fyne.io/fyne/v2/container"
    "fyne.io/fyne/v2/theme"
    "image/color"
)

func main() {
    // ToDo:
    // Layout issues -> not my cup of tee: https://github.com/fyne-io/fyne/issues/2704
    // create own theme
    // optimize sound quality
    var (
        timeBoxWidth   float32 = 200
        timeBoxHeight  float32 = 180
        rotiBoxWidth   float32 = 200 //250
        rotiBoxHeight  float32 = 360
        clockTextSize  float32 = 32
        clockTextColor color.Color
    )

    a := app.NewWithID("time box")
    a.Settings().SetTheme(theme.DarkTheme())

    w := a.NewWindow("We Meet Today")
    //w.SetFixedSize(true)

    clockTextColor = theme.ForegroundColor()
    notification := fyne.NewNotification("Time Box", "The time is up!")
    timeBox := newTimeBox(timeBoxWidth, timeBoxHeight, clockTextSize, clockTextColor, func() {
        a.SendNotification(notification)
    })

    rotiCalculator := newROTICalculator(rotiBoxWidth, rotiBoxHeight)

    tabsContainer := container.NewAppTabs(
        container.NewTabItem("Time Box", timeBox),
        container.NewTabItem("ROTI", rotiCalculator),
    )
    tabsContainer.OnSelected = func(item *container.TabItem) {
        newWidth := timeBoxWidth
        newHeight := timeBoxHeight

        if item.Text == "ROTI" {
            newWidth = rotiBoxWidth
            newHeight = rotiBoxHeight
        }
        w.Resize(fyne.NewSize(newWidth, newHeight))
    }

    w.SetContent(tabsContainer)

    w.Resize(fyne.NewSize(timeBoxWidth, timeBoxHeight))

    w.ShowAndRun()
}
