package main

import (
    "fmt"
    "fyne.io/fyne/v2"
    "fyne.io/fyne/v2/canvas"
    "fyne.io/fyne/v2/driver/desktop"
    "fyne.io/fyne/v2/theme"
    "fyne.io/fyne/v2/widget"
    "image/color"
)

var (
    defTextStyle = fyne.TextStyle{
        Bold:      false,
        Italic:    false,
        Monospace: false,
    }
)

type SpinSwiper struct {
    widget.BaseWidget
    desktop.Hoverable // for MouseIn and MouseOut
    number            int
    textSize          float32
    textStyle         fyne.TextStyle
    textColor         color.Color
    background        color.Color

    isEditing bool
    focused   bool

    OnTapped          func()                    `json:"-"`
    OnTappedSecondary func()                    `json:"-"`
    OnDoubleTapped    func()                    `json:"-"`
    OnScrolled        func(e *fyne.ScrollEvent) `json:"-"`
    OnStartEditing    func()                    `json:"-"`
    OnEdited          func(number int)          `json:"-"`
}

type SpinSwiperRenderer struct {
    text *canvas.Text
    bg   *canvas.Rectangle

    objects []fyne.CanvasObject
    combo   *SpinSwiper
}

func (s *SpinSwiper) EditMode() {
    // on start of editing
    if !s.isEditing {
        s.OnStartEditing()
    }
    // on end of editing...
    if s.isEditing {
        s.OnEdited(s.number)
    }

    s.isEditing = !s.isEditing
    s.Refresh()
}

func (s *SpinSwiper) Inc() {
    s.number++
    if s.number > 59 {
        s.number = 59
    }

    s.Refresh()
}

func (s *SpinSwiper) Dec() {
    s.number--
    if s.number < 0 {
        s.number = 0
    }

    s.Refresh()
}

func newSpinSwiper(number int, textSize float32, col color.Color, bg color.Color) *SpinSwiper {
    s := &SpinSwiper{
        number:     number,
        textSize:   textSize,
        textStyle:  defTextStyle,
        textColor:  col,
        background: bg,
    }
    s.ExtendBaseWidget(s)

    return s
}

func (s *SpinSwiper) CreateRenderer() fyne.WidgetRenderer {
    s.ExtendBaseWidget(s)

    bg := &canvas.Rectangle{
        FillColor:   s.background,
        StrokeColor: nil,
        StrokeWidth: 0,
    }

    t := &canvas.Text{
        Alignment: fyne.TextAlignCenter,
        Color:     s.textColor,
        Text:      toTwoDigits(s.number),
        TextSize:  s.textSize,
        TextStyle: s.textStyle,
    }

    objects := []fyne.CanvasObject{bg, t}
    r := &SpinSwiperRenderer{
        text:    t,
        bg:      bg,
        objects: objects,
        combo:   s,
    }

    return r
}

func (s *SpinSwiperRenderer) Destroy() {}

func (s *SpinSwiperRenderer) Layout(size fyne.Size) {
    s.text.Resize(size)
    s.bg.Resize(size)
}

func (s *SpinSwiperRenderer) MinSize() fyne.Size {
    // workaround for scaling issue on double click -> bold text
    if s.text.TextStyle.Bold {
        return fyne.NewSize(0, 0)
    }

    min := s.text.MinSize()
    return min
}

func (s *SpinSwiperRenderer) Objects() []fyne.CanvasObject {
    return s.objects
}

func (s *SpinSwiperRenderer) Refresh() {
    s.text.Text = toTwoDigits(s.combo.number)
    s.text.Color = s.combo.textColor
    s.text.TextSize = s.combo.textSize
    s.text.TextStyle = s.combo.textStyle

    if s.combo.isEditing {
        s.text.Color = theme.PrimaryColor()
        s.text.TextSize = s.combo.textSize + 2
        s.text.TextStyle.Bold = true
    }

    s.bg.FillColor = s.combo.background

    canvas.Refresh(s.combo)
}

func (s *SpinSwiper) Tapped(_ *fyne.PointEvent) {
    if s.OnTapped != nil {
        s.OnTapped()
    }
}

func (s *SpinSwiper) TappedSecondary(_ *fyne.PointEvent) {
    if s.OnTappedSecondary != nil {
        s.OnTappedSecondary()
    }
}

func (s *SpinSwiper) DoubleTapped(_ *fyne.PointEvent) {
    s.EditMode()

    if s.OnDoubleTapped != nil {
        s.OnDoubleTapped()
    }
}

func (s *SpinSwiper) Scrolled(e *fyne.ScrollEvent) {
    if s.isEditing {
        if e.Scrolled.DY > 0 {
            s.Inc()
        } else {
            s.Dec()
        }
        s.Refresh()
    }

    if s.OnScrolled != nil {
        s.OnScrolled(e)
    }
}

func (s *SpinSwiper) MouseMoved(event *desktop.MouseEvent) {}

func (s *SpinSwiper) MouseIn(e *desktop.MouseEvent) {}

func (s *SpinSwiper) MouseOut() {
    if s.isEditing {
        s.isEditing = !s.isEditing
        s.Refresh()
        s.OnEdited(s.number)
    }
}

func toTwoDigits(num int) string {
    return fmt.Sprintf("%02d", num)
}
