package main

import (
    "fyne.io/fyne/v2"
    "fyne.io/fyne/v2/canvas"
    "fyne.io/fyne/v2/container"
    "fyne.io/fyne/v2/theme"
    "fyne.io/fyne/v2/widget"
    "image/color"
    "strconv"
)

func newTimeBox(width float32, height float32, textSize float32, textColor color.Color, onTimeIsUp func()) *fyne.Container {
    var (
        colorText     = textColor
        colorTimeIsUp = theme.ErrorColor()
        colorTimerBar = theme.PrimaryColor()

        countdown               *Countdown
        spacing                 float32 = 2.0
        remainingTimePercentage float32 = 1.0

        OnTimeIsUp = onTimeIsUp
    )

    timeBoxContainer := container.NewWithoutLayout()
    timeBoxContainer.Resize(fyne.NewSize(width, height))

    hour := newSpinSwiper(0, textSize, colorText, color.Transparent)
    hour.Resize(fyne.NewSize(hour.MinSize().Width+spacing, hour.MinSize().Height+spacing))

    colon1 := &canvas.Text{
        Alignment: fyne.TextAlignCenter,
        Color:     colorText,
        Text:      ":",
        TextSize:  textSize,
        TextStyle: defTextStyle,
    }
    colon1.Resize(colon1.MinSize())

    min := newSpinSwiper(5, textSize, colorText, color.Transparent)
    min.Resize(fyne.NewSize(min.MinSize().Width+spacing, min.MinSize().Height+spacing))

    colon2 := &canvas.Text{
        Alignment: fyne.TextAlignCenter,
        Color:     colorText,
        Text:      ":",
        TextSize:  textSize,
        TextStyle: defTextStyle,
    }
    colon2.Resize(colon2.MinSize())

    sec := newSpinSwiper(0, textSize, colorText, color.Transparent)
    sec.Resize(fyne.NewSize(sec.MinSize().Width+spacing, sec.MinSize().Height+spacing))

    timerBarBG := &canvas.Rectangle{
        FillColor:   theme.DisabledColor(),
        StrokeColor: nil,
        StrokeWidth: 0,
    }

    timerBar := &canvas.Rectangle{
        FillColor:   theme.PrimaryColor(),
        StrokeColor: nil,
        StrokeWidth: 0,
    }

    // buttons
    hourLabel := &canvas.Text{
        Alignment: fyne.TextAlignCenter,
        Color:     color.White,
        Text:      "H",
        TextSize:  10,
        TextStyle: defTextStyle,
    }

    minLabel := &canvas.Text{
        Alignment: fyne.TextAlignCenter,
        Color:     color.White,
        Text:      "MIN",
        TextSize:  10,
        TextStyle: defTextStyle,
    }

    secLabel := &canvas.Text{
        Alignment: fyne.TextAlignCenter,
        Color:     color.White,
        Text:      "SEC",
        TextSize:  10,
        TextStyle: defTextStyle,
    }

    t := container.NewGridWithColumns(4)

    buttonTimeslot1 := newExtendedButton("5")
    t.Add(buttonTimeslot1)

    buttonTimeslot2 := newExtendedButton("15")
    t.Add(buttonTimeslot2)

    buttonTimeslot3 := newExtendedButton("45")
    t.Add(buttonTimeslot3)

    buttonStartPause := &widget.Button{
        Icon: theme.MediaPlayIcon(),
    }

    onTappedFunc := func(seconds int) {
        h, m, s := Seconds2HMS(seconds)
        hour.number = h
        min.number = m
        sec.number = s

        countdown.Set(hour.number, min.number, sec.number)

        hour.textColor = colorText
        min.textColor = colorText
        sec.textColor = colorText

        ScaleBarByTime(timerBar, timerBarBG, 1.0)
        timerBar.FillColor = colorTimerBar

        buttonStartPause.Enable()

        timeBoxContainer.Refresh()
    }

    callbackFunc := func() {
        for {
            select {
            case <-countdown.tickerDone:
                return

            case <-countdown.ticker.C:
                countdown.seconds--
                countdown.OnTick(countdown.seconds)
            }
        }
    }

    onTickFunc := func(seconds int) {
        h, m, s := Seconds2HMS(seconds)
        hour.number = h
        min.number = m
        sec.number = s

        if countdown.seconds > 0 {
            remainingTimePercentage = float32(countdown.seconds) / float32(countdown.initialHour*3600+countdown.initialMin*60+countdown.initialSec)
        } else {
            remainingTimePercentage = 1.0

            hour.textColor = colorTimeIsUp
            min.textColor = colorTimeIsUp
            sec.textColor = colorTimeIsUp

            timerBar.FillColor = colorTimeIsUp
        }
        ScaleBarByTime(timerBar, timerBarBG, remainingTimePercentage)
        timeBoxContainer.Refresh()

        if seconds < 4 && seconds > 0 {
            go playWavFile("beep-24.wav", 1)
        }

        if seconds <= 0 {
            countdown.Stop()
            buttonStartPause.SetIcon(theme.MediaPlayIcon())
            buttonStartPause.Disable()

            buttonTimeslot1.Enable()
            buttonTimeslot2.Enable()
            buttonTimeslot3.Enable()

            go playWavFile("beep-24.wav", 5)
            go OnTimeIsUp() // so main can send notification message
        }
    }

    countdown = newCountdown(hour.number, min.number, sec.number)

    // ---- events ----
    hour.OnStartEditing = func() {
        hour.textColor = colorText
        hour.Refresh()
        min.textColor = colorText
        min.Refresh()
        sec.textColor = colorText
        sec.Refresh()
        timerBar.FillColor = colorTimerBar
        timerBar.Refresh()
    }
    hour.OnEdited = func(num int) {
        countdown.SetHour(num)
        ScaleBarByTime(timerBar, timerBarBG, 1.0)
        buttonStartPause.Enable()
    }

    min.OnStartEditing = func() {
        hour.textColor = colorText
        hour.Refresh()
        min.textColor = colorText
        min.Refresh()
        sec.textColor = colorText
        sec.Refresh()
        timerBar.FillColor = colorTimerBar
        timerBar.Refresh()
    }
    min.OnEdited = func(num int) {
        countdown.SetMin(num)
        ScaleBarByTime(timerBar, timerBarBG, 1.0)
        buttonStartPause.Enable()
    }

    sec.OnStartEditing = func() {
        hour.textColor = colorText
        hour.Refresh()
        min.textColor = colorText
        min.Refresh()
        sec.textColor = colorText
        sec.Refresh()
        timerBar.FillColor = colorTimerBar
        timerBar.Refresh()
    }
    sec.OnEdited = func(num int) {
        countdown.SetSec(num)
        ScaleBarByTime(timerBar, timerBarBG, 1.0)
        buttonStartPause.Enable()
    }

    buttonTimeslot1.OnTapped = func() {
        m, _ := strconv.Atoi(buttonTimeslot1.Text)
        onTappedFunc(m * 60)
    }
    buttonTimeslot1.OnTappedSecondary = func() {
        buttonTimeslot1.Text = strconv.Itoa(hour.number*60 + min.number)
        buttonTimeslot1.Refresh()
    }

    buttonTimeslot2.OnTapped = func() {
        m, _ := strconv.Atoi(buttonTimeslot2.Text)
        onTappedFunc(m * 60)
    }
    buttonTimeslot2.OnTappedSecondary = func() {
        buttonTimeslot2.Text = strconv.Itoa(hour.number*60 + min.number)
        buttonTimeslot2.Refresh()
    }

    buttonTimeslot3.OnTapped = func() {
        m, _ := strconv.Atoi(buttonTimeslot3.Text)
        onTappedFunc(m * 60)
    }
    buttonTimeslot3.OnTappedSecondary = func() {
        buttonTimeslot3.Text = strconv.Itoa(hour.number*60 + min.number)
        buttonTimeslot3.Refresh()
    }

    buttonStartPause.OnTapped = func() {
        if !countdown.isRunning && !countdown.isTimeIsUp {
            playWavFile("beep-24.wav", 1)
            buttonStartPause.Icon = theme.MediaPauseIcon()

            buttonTimeslot1.Disable()
            buttonTimeslot2.Disable()
            buttonTimeslot3.Disable()

            timeBoxContainer.Refresh()

            countdown.Start(callbackFunc, onTickFunc)

        } else {
            countdown.Stop()
            buttonStartPause.Icon = theme.MediaPlayIcon()

            buttonTimeslot1.Enable()
            buttonTimeslot2.Enable()
            buttonTimeslot3.Enable()

            timeBoxContainer.Refresh()
        }

    }
    t.Add(buttonStartPause)

    /* ---------------
     *    placement
     * ---------------*/
    sumWidth := hour.Size().Width + colon1.Size().Width + min.Size().Width + colon2.Size().Width + sec.Size().Width + 4*spacing
    // position hour absolutely
    hour.Move(fyne.NewPos(width/2-sumWidth/2, height/2-hour.Size().Height-spacing))
    // now position relatively to hour
    colon1.Move(fyne.NewPos(hour.Position().X+hour.Size().Width, hour.Position().Y))
    min.Move(fyne.NewPos(colon1.Position().X+colon1.Size().Width, colon1.Position().Y))
    colon2.Move(fyne.NewPos(min.Position().X+min.Size().Width, min.Position().Y))
    sec.Move(fyne.NewPos(colon2.Position().X+colon2.Size().Width, colon2.Position().Y))

    timerBarBG.Resize(fyne.NewSize(sumWidth, 13))
    timerBarBG.Move(fyne.NewPos(hour.Position().X, hour.Position().Y+hour.Size().Height-5*spacing))

    timerBar.Resize(fyne.NewSize(sumWidth, 13))
    timerBar.Move(fyne.NewPos(hour.Position().X, hour.Position().Y+hour.Size().Height-5*spacing))

    hourLabel.Move(fyne.NewPos(hour.Position().X+hour.Size().Width/2-hourLabel.Size().Width/2, timerBarBG.Position().Y))
    minLabel.Move(fyne.NewPos(min.Position().X+min.Size().Width/2-minLabel.Size().Width/2, timerBarBG.Position().Y))
    secLabel.Move(fyne.NewPos(sec.Position().X+sec.Size().Width/2-secLabel.Size().Width/2, timerBarBG.Position().Y))

    t.Resize(fyne.NewSize(sumWidth, buttonTimeslot1.MinSize().Width))
    t.Move(fyne.NewPos(hour.Position().X, timerBar.Position().Y+timerBar.Size().Height+2*spacing))

    timeBoxContainer.Add(hour)
    timeBoxContainer.Add(colon1)
    timeBoxContainer.Add(min)
    timeBoxContainer.Add(colon2)
    timeBoxContainer.Add(sec)
    timeBoxContainer.Add(timerBarBG)
    timeBoxContainer.Add(timerBar)
    timeBoxContainer.Add(hourLabel)
    timeBoxContainer.Add(minLabel)
    timeBoxContainer.Add(secLabel)
    timeBoxContainer.Add(t)

    return timeBoxContainer
}

func ScaleBarByTime(bar *canvas.Rectangle, barBG *canvas.Rectangle, scale float32) {
    if scale < 0.0 {
        scale = 0.0
    }
    if scale > 1.0 {
        scale = 1.0
    }
    bar.Resize(fyne.NewSize(-1*(barBG.Size().Width-barBG.Size().Width*(1-scale)), barBG.Size().Height))
    //bar.Move(fyne.NewPos(barBG.Position().X+barBG.Size().Width*scale, barBG.Position().Y))
    bar.Move(fyne.NewPos(barBG.Position().X+barBG.Size().Width, barBG.Position().Y))
    //bar.Refresh()
}
