package main

import (
    "math"
    "time"
)

type Countdown struct {
    initialHour int
    initialMin  int
    initialSec  int
    seconds     int
    isRunning   bool
    isTimeIsUp  bool
    ticker      *time.Ticker
    tickerDone  chan bool

    OnTick func(seconds int) `json:"-"`
}

func newCountdown(hour int, min int, sec int) *Countdown {
    c := &Countdown{
        initialHour: hour,
        initialMin:  min,
        initialSec:  sec,
        seconds:     hour*3600 + min*60 + sec,
        isRunning:   false,
        isTimeIsUp:  false,
        ticker:      time.NewTicker(time.Second * 1),
        tickerDone:  make(chan bool),
        OnTick:      nil,
    }

    return c
}

func (c *Countdown) Set(hour int, min int, sec int) {
    c.initialHour = hour
    c.initialMin = min
    c.initialSec = sec
    c.seconds = c.initialHour*3600 + c.initialMin*60 + c.initialSec
    c.isTimeIsUp = false
}

func (c *Countdown) SetHour(hour int) {
    _, m, s := Seconds2HMS(c.seconds)
    c.initialHour = hour
    c.initialMin = m
    c.initialSec = s
    c.seconds = c.initialHour*3600 + c.initialMin*60 + c.initialSec
    c.isTimeIsUp = false
}

func (c *Countdown) SetMin(min int) {
    h, _, s := Seconds2HMS(c.seconds)
    c.initialHour = h
    c.initialMin = min
    c.initialSec = s
    c.seconds = c.initialHour*3600 + c.initialMin*60 + c.initialSec
    c.isTimeIsUp = false
}

func (c *Countdown) SetSec(sec int) {
    h, m, _ := Seconds2HMS(c.seconds)
    c.initialHour = h
    c.initialMin = m
    c.initialSec = sec
    c.seconds = c.initialHour*3600 + c.initialMin*60 + c.initialSec
    c.isTimeIsUp = false
}

func (c *Countdown) Start(callback func(), onTick func(sec int)) {
    if !c.isRunning {
        c.ticker = time.NewTicker(time.Second * 1)
        c.isRunning = true
        c.tickerDone = make(chan bool)
        c.OnTick = onTick

        go callback()
    }
}

func (c *Countdown) Stop() {
    c.ticker.Stop()
    c.isRunning = false
}

func Seconds2HMS(seconds int) (int, int, int) {
    var r float64

    h := math.Floor(float64(seconds) / 3600) //  Math.floor(seconds / 3600)
    r = float64(seconds) - h*3600
    m := math.Floor(r / 60)
    s := r - m*60

    return int(h), int(m), int(s)
}
