package main

import (
    "fyne.io/fyne/v2"
    "fyne.io/fyne/v2/driver/mobile"
    "fyne.io/fyne/v2/widget"
    "strconv"
    "strings"
)

type numEntry struct {
    widget.Entry
}

func newNumEntry() *numEntry {
    entry := &numEntry{}
    entry.ExtendBaseWidget(entry)
    return entry
}

func (e *numEntry) TypedRune(r rune) {
    switch r {
    case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
        e.Entry.TypedRune(r)
    }
}

func (e *numEntry) TypedShortcut(shortcut fyne.Shortcut) {
    paste, ok := shortcut.(*fyne.ShortcutPaste)
    if !ok {
        e.Entry.TypedShortcut(shortcut)
        return
    }

    content := paste.Clipboard.Content()
    if _, err := strconv.ParseFloat(content, 64); err == nil {
        e.Entry.TypedShortcut(shortcut)
    }
}

func (e *numEntry) Keyboard() mobile.KeyboardType {
    return mobile.NumberKeyboard
}

func (e *numEntry) Rows() []string {
    lines := strings.Split(e.Text, "\n")
    return lines
}

func (e *numEntry) NumRows() int {
    return len(e.Rows())
}

// NonEmptyRows returns sum and count of rows of numEntry
func (e *numEntry) NonEmptyRows() (int, int) {
    var count = 0
    var sum = 0

    lines := e.Rows()
    for _, line := range lines {
        if line != "" {
            count++
            n, _ := strconv.Atoi(line)
            sum += n
        }

    }
    return sum, count
}
