package main

import (
    "fyne.io/fyne/v2"
    "fyne.io/fyne/v2/widget"
)

type extendedButton struct {
    widget.Button

    OnTapped          func() `json:"-"`
    OnTappedSecondary func() `json:"-"`
}

func newExtendedButton(text string) *extendedButton {
    but := &extendedButton{}
    but.Text = text
    but.ExtendBaseWidget(but)
    return but
}

func (b *extendedButton) Tapped(_ *fyne.PointEvent) {
    if b.OnTapped != nil {
        b.OnTapped()
    }
}

func (b *extendedButton) TappedSecondary(_ *fyne.PointEvent) {
    if b.OnTappedSecondary != nil {
        b.OnTappedSecondary()
    }
}
